use std::time::{Duration, SystemTime};

use anyhow::{Context, Result};
use rppal::spi::{Bus, Mode, SlaveSelect, Spi};
use serde::Serialize;
use tokio::time::sleep;

#[derive(Serialize)]
struct Row {
    time: SystemTime,
    measurment: u16,
}

impl Row {
    fn new(measurment: u16) -> Self {
        let time = SystemTime::now();
        Self { time, measurment }
    }
}

fn createmcp() -> Result<mcp3xxx::Mcp3002> {
    let spi = Spi::new(Bus::Spi0, SlaveSelect::Ss0, 100000, Mode::Mode0)
        .with_context(|| "failed to create spi connection")?;
    mcp3xxx::Mcp3002::new(spi).with_context(|| "Failed to create mcp object from spi")
}

async fn measeure(mcp: &mut mcp3xxx::Mcp3002) -> u16 {
    mcp.single_ended_read(mcp3xxx::Channel::new(0).unwrap())
        .unwrap()
        .value()
}

#[tokio::main]
async fn main() -> Result<()> {
    // let mut mcp = createmcp()?;
    let name = uuid7::uuid7();
    let mut csv_writer = csv::WriterBuilder::new().from_path(&format!("./{}.csv", name))?;

    loop {
        // let row = Row::new(measeure(&mut mcp).await);
        let record = csv_writer.serialize(row)?;
        // csv_writer.flush();
        sleep(Duration::new(5, 0)).await;
    }

    // Ok(())
}
